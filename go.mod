module gitlab.com/okotek/okolog

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	gitlab.com/okotech/loadoptions v0.0.0-20220331051017-d846c01d796e
	gitlab.com/okotek/okotypes v0.0.0-20220328184655-40ed17237fbd
	gitlab.com/okotek/sec v0.0.0-20220326221119-e10389566a6b
)

require golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
