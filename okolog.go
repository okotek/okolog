package okolog

import (
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"

	loadOptions "gitlab.com/okotech/loadoptions"
	types "gitlab.com/okotek/okotypes"
	sec "gitlab.com/okotek/sec"
)

type instanceStruct struct {
	InitTime   time.Time
	Identifier string
	FilePath   string
	UserName   string
	CamID      string
	UnitName   string
}

var InitStruct instanceStruct //This is initialized at runtime and stores a particular instance's metadata

/* */
type sendoffStruct struct {
	MessageClass string
	Message      string
	ExeName      string
	UserName     string
	CamID        string
	UnitName     string
}

// MessageLogger logs a simple string (SEND)
func MessageLogger(input string, class string) {

	validateClass(&class)

	//fmt.Println("📰📰📰Sending: ", input)

	cfg := loadOptions.Load()
	failCount := 0
	var protoMessage sendoffStruct

	exeNameProto := strings.Split(InitStruct.FilePath, "/")
	exeName := exeNameProto[len(exeNameProto)-1]

	input = fmt.Sprintf("%d,%s,%s,%s", time.Now().UnixNano(), InitStruct.FilePath, input, InitStruct.Identifier)
	protoMessage.Message = input
	protoMessage.ExeName = exeName
	protoMessage.UserName = cfg.EyeballUserName
	protoMessage.UnitName = cfg.EyeballUnitID
	protoMessage.MessageClass = class

	//This tries to dial the logger server fifty times before giving up and dying.
	for {
		conn, err := net.Dial("tcp", cfg.StringLogAddress)
		//fmt.Println("\n\nStringLogAddress: ", cfg.StringLogAddress)

		if err != nil {
			log.Println("Error in Message Logger: ", err, "--", cfg.StringLogAddress)
			failCount++
			fmt.Println("Failcount is :", failCount)
			if failCount > 49 {
				fmt.Printf("\n\n\n%d is too many failures!\n\n", failCount)
				return
			}
			continue
		}

		enc := gob.NewEncoder(conn)
		enc.Encode(protoMessage)
		conn.Close()
		return
	}
}

func CountLogger(input string, class string) {
	validateClass(&class)
	cfg := loadOptions.Load()

	var protoMessage sendoffStruct
	exeNameProto := strings.Split(InitStruct.FilePath, "/")
	exeName := exeNameProto[len(exeNameProto)-1]

	input = fmt.Sprintf("%d, %s, %s, %s,", time.Now().UnixNano(), InitStruct.FilePath, input, InitStruct.Identifier)
	protoMessage.Message = input
	protoMessage.ExeName = exeName
	protoMessage.UserName = cfg.EyeballUserName
	protoMessage.UnitName = cfg.EyeballUnitID
	protoMessage.MessageClass = class

	for {
		con, err := net.Dial("tcp", cfg.CounterListenAddress)

		if err != nil {
			fmt.Println("Error in CountLogger with dial: ", err)
			continue
		}

		enc := gob.NewEncoder(con)
		enc.Encode(protoMessage)
		con.Close()
		return

	}
}

func CountLoggerListener() {
	cfg := loadOptions.Load()

	var tmpMsg sendoffStruct
	fmt.Printf("CountLoggerListener listening on %s, port: %s", cfg.CounterListenAddress, cfg.CounterListenPort)
	for {
		ln, err := net.Listen("tcp", cfg.CounterListenPort)
		if err != nil {
			for {
				con, err := ln.Accept()
				if err != nil {
					fmt.Println("ERROR IN COUNT LOGGER LISTENER ACCEPT FUNCTION: ", err)
				}

				dec := gob.NewDecoder(con)
				err = dec.Decode(&tmpMsg)

				ioutil.WriteFile("./logs/"+tmpMsg.CamID+tmpMsg.ExeName+tmpMsg.UserName+tmpMsg.Message+tmpMsg.MessageClass+".csv", []byte(strconv.Itoa(int(time.Now().UnixNano()))), 0777)
			}
		}
	}
}

//StringLoggerListener listens for incoming string logs (REC)
func StringLoggerListener() {
	cfg := loadOptions.Load()
	fmt.Println("Config", cfg)

	var tmpMsg sendoffStruct

	for {
		ln, err2 := net.Listen("tcp", cfg.StringListenAddress)
		fmt.Print("\n\nStringListen: ", cfg.StringListenAddress, "\n\n")
		if err2 != nil {
			fmt.Print("\n\nBig error in StringLoggerListener: ", err2, "\n\n")
			continue
		}

		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Println("ERROR: StringLoggerListener: ", err, "--", cfg.StringListenAddress)
			}

			dec := gob.NewDecoder(conn)
			err = dec.Decode(&tmpMsg)
			conn.Close()

			if err != nil {
				fmt.Println("ERROR: StringLoggerListener: ", err, "--", cfg.StringListenAddress)
				continue
			}

			toSend := fmt.Sprintf("Message: %s, Executable Name:%s, UserName:%s, CamID:%s, UnitName:%s\n", tmpMsg.Message, tmpMsg.ExeName, tmpMsg.UserName, tmpMsg.CamID, tmpMsg.UnitName)

			finName := strings.Split(tmpMsg.ExeName, "/")
			targFile := "./logs/oko_" + finName[0] + ".csv"

			go writeToFile(targFile, toSend)

			//go writeToFile("/home/andrew/StringLog.csv", toSend)
			go writeToDaba("okoLog", "password", toSend, "10.0.3.229:3306")
		}
	}

}

//LoggerIDInit sets up for logging in this process
func LoggerIDInit(input types.ConfigOptions) {
	InitStruct.InitTime = time.Now()
	InitStruct.Identifier = sec.HashAndSalt(strconv.Itoa(int(InitStruct.InitTime.UnixNano())))
	InitStruct.FilePath, _ = os.Executable()

	InitStruct.UserName = input.EyeballUserName
	InitStruct.CamID = input.EyeballUnitID
}

//Write logs to flat file
func writeToFile(fileName string, data string) {
	for {
		handle, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			os.Create(fileName)
			continue
		} else {
			tNow := time.Now()
			data = tNow.Format(time.RFC3339) + ", " + data
			_, err = handle.WriteString(data)
			handle.Close()
			break
		}

	}
}

//Write logs to database
func writeToDaba(username string, password string, data string, sqlServer string) { /*
		db, err := sql.Open("mysql", "username:password@tcp(127.0.0.1:3306)/test")

		// if there is an error opening the connection, handle it
		if err != nil {
			panic(err.Error())
		}

		// defer the close till after the main function has finished
		// executing
		defer db.Close()

		insert, err := db.Query("INSERT INTO logging VALUES ( 2, 'TEST' )")

		// if there is an error inserting, handle it
		if err != nil {
			panic(err.Error())
		}
		// be careful deferring Queries if you are using transactions
		defer insert.Close()
	*/
	fmt.Println(username, password, data, sqlServer)
}

func validateClass(input *string) {
	if "error" == strings.ToLower(*input) {
		*input = "ERROR"
		return
	} else if "log" == strings.ToLower(*input) {
		*input = "LOG"
		return
	} else {
		*input = *input + " -- INVALID MESSAGE CLASS"
	}
}
